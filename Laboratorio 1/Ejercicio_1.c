/*Integrantes
    Renata Arcos 
    Bastián Morales
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/errno.h>

#define N 7

int main(){
    pid_t hijos[N];
    int i;

    for (i = 0; i < N; i++){
        hijos[i] = fork();//Se crean los hijos

        if( hijos[i] == 0){//Entran solo los hijos
            printf("Hijo - N° %d Pid = %d\n", i+1 , getpid());
            break;//se rompe para que en el siguiente ciclo solo el padre cree 1 hijo
        }
    }
    //una vez se llega al numero de hijos deseados, se muestra el pid del padre
    if ( i == N){
        printf("Padre - Pid = %d\n", getpid());//el padre cuenta como 1 proceso más
    }
    return 0;
}