/*Integrantes
    Renata Arcos 
    Bastián Morales
*/
#include <stdio.h>
#include <pthread.h>

//define la cantidad de hebras que se crearán
#define NUM_THREADS 8

//en esta función se imprimen los mensajes de las hebras creadas
void *imprimir_mensaje(void *arg) {
  int id_hebra = *((int*) arg); 
  printf("Hola desde la hebra %d\n", id_hebra+1);
  pthread_exit(NULL);
}

int main() {
  //aquí se guardarán los identificadores de las hebras
  pthread_t hebras[NUM_THREADS];
  //aquí se guardan los ids de las hebras
  int ids[NUM_THREADS];

  //en este for se crea la cantidad deseada de hebras
  for (int i = 0; i < NUM_THREADS; i++) {
    ids[i] = i; //su id corresponderá con i
    //en este segmento se crean las hebras
    pthread_create(&hebras[i], NULL, imprimir_mensaje, (void *) &ids[i]);
    //y se espera a que finalice su proceso para continuar
    pthread_join(hebras[i], NULL);
  }


  return 0;
}