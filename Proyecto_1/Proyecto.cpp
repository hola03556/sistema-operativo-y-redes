/*
- - - Integrantes: Renata Arcos - Bastián Morales - - -

Presentacion del Proyecto: 
Este proyecto consiste en la implementación de un problema clásico de sincronización
de procesos, el cual es conocido como el problema del productor-consumidor. Para 
resolver este problema, se ha utilizado el lenguaje de programación C++ y se han utilizado
hilos, semáforos y mutex para lograr una sincronización eficiente entre los productores y consumidores.

Cosas a tener en cuenta:

1. La cantidad de productos debe ser suficiente para satisfacer a todos los consumidores.
2. La cantidad de productos en total no debe superar a los productos a consumir más la capacidad de
   almacenamiento del buffer.
3. La capacidad del buffer no puede ser 0.
4. La cantidad de argumentos a recibir por la línea de comandos debe ser igual a 6 (contando el archivo
   ejecutable).
*/

// Librerias 
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <mutex>

using namespace std;
int NP, NC, NPP, NCC, BC, count; // variables globales que almacenan los argumentos del programa
vector<string> buffer; // vector que representa el buffer
mutex mtx; // objeto de clase mutex para sincronizar el acceso al buffer
sem_t fulls; // semáforo que indica el número de espacios ocupados en el buffer
sem_t emptys; // semáforo que indica el número de espacios vacíos en el buffer


// Función que simula el comportamiento de un productor
void func_productor(int I){

    int ids = I + 1;
    int contador_prod = 0; // Ayuda a saber la cantidad de productos creados
    string data;
    mtx.lock(); // Bloquea el acceso al buffer
    for(int i = 0; i < NPP; i++){
        if(contador_prod == i){/* Verifica que la cantidad de productos creados sea igual
                                  al iterador. Si son iguales significa que es un producto
                                  nuevo, y se imprime que fue creado. Si no son iguales,
                                  significa que se trata de un reintento de insertar el producto
                                  en el buffer.*/
            data = "* Productor " +  to_string(ids) + " * " + "genero Producto " + to_string(ids)  + "_" + to_string(i+1);
            cout << data << endl;
            contador_prod++;
        }
        data = to_string(ids) + "_" + to_string(i+1);
        if (buffer.size() < BC){ /*verifica si el tamaño del buffer es menor que la capacidad máxima.
                                   Si es así, agrega el producto generado al final del buffer y luego 
                                   aumenta el semáforo fulls. Si el tamaño del buffer es igual o mayor
                                   que la capacidad máxima, el productor no puede insertar el producto.*/
            sem_wait(&emptys); // Decrementa el semáforo de espacios vacíos
            buffer.push_back(data); // Inserta el elemento en el buffer
            data = "* Productor " + to_string(ids) + " * Producto " + data + " insercion exitosa.";
            cout << data << endl;
            sem_post(&fulls); // Incrementa el semáforo de espacios ocupados
            mtx.unlock(); // Libera el acceso al buffer
            sleep(rand() % 5); // Espera un tiempo aleatorio
            
        }else{
            data = "Productor " + to_string(ids) + " " + data + " Buffer lleno - Error de Insercion.";
            cout << data << endl;
            mtx.unlock(); // Libera el acceso al buffer
            sleep(rand()% 5);
            i--; // Vuelve a intentar insertar el mismo elemento
            continue; // Continua con el siguiente ciclo
        }
    }
    contador_prod = 0;
    cout << "**************************" << endl;
    data = "# Productor " + to_string(ids) + " terminado. #";
    cout << data << endl;
    cout << "**************************" << endl;
}

// Función que simula el comportamiento de un consumidor
void func_consumidor(int I){

    int ids = I + 1;
    string data;
     for(int i = 0; i < NCC; i++){
        mtx.lock();  
        data =  "* Consumidor " + to_string(ids) + " * " + "intentando eliminar el elemento del buffer.";
        cout << data << endl;
        if (!buffer.empty()){/*Si el buffer está vacío, la función simplemente imprime un 
                               mensaje indicando que el buffer está vacío y se duerme por un 
                               tiempo aleatorio antes de intentar nuevamente. Si el buffer no 
                               está vacío, el consumidor elimina el primer elemento del buffer,
                               actualiza el mensaje de datos y lo imprime,*/
            string consumido = buffer.front(); // Obtiene el primer elemento del buffer
            buffer.erase(buffer.begin()); // Elimina el primer elemento del buffer
            sem_wait(&fulls); // Decrementa el semáforo de espacios ocupados
            data = "Consumidor " + to_string(ids) + " * " + "Producto " + consumido + " eliminado correctamente.";
            cout << data << endl;
            sem_post(&emptys); // Incrementa el semáforo de espacios vacíos
            mtx.unlock(); // Libera el acceso al buffer
            sleep(rand() % 5);

        }else{
            mtx.unlock();
            data = " Buffer vacío - Error de Eliminación - Consumidor " + to_string(ids) + " espera.";
            cout << data << endl;
            sleep(rand()% 5);
            i--; // Vuelve a intentar insertar el mismo elemento
            continue; // Continua con el siguiente ciclo
        }
    }
    cout << "###########################" << endl;
    data = "# Consumidor " + to_string(ids) + " terminado. #";
    cout << data << endl;
    cout << "###########################" << endl;    
}



int main(int argc, char *argv[]){

    srand(time(NULL));
    if (argc != 6) {
        cout << "Error: debes introducir seis números como argumentos" << endl;
        return 0;
    }
    NP = atoi(argv[1]); 
    NC = atoi(argv[2]); 
    BC = atoi(argv[3]);
    NPP = atoi(argv[4]); 
    NCC = atoi(argv[5]);

    // Los parametros cumplan con las condiciones necesarias
    if ( NC*NCC > NP*NPP){
        cout << "/t # # # Argumentos Inválidos # # #" << endl;
        cout << "* Cantidad de productos a consumir es mayor de la cantidad de productos producidos" << endl;
        return 0;
    }else if(NP*NPP > NC*NCC + BC){
        cout << "/t # # # Argumentos Inválidos # # #" << endl;
        cout << "¡¡ La cantidad de productos producidos excede la cantidad de demanda y la capacidad del buffer !!" << endl;
        return 0;
    }else if( BC == 0){
        cout << "/t # # # Argumentos Inválidos # # #" << endl;
        cout << "* Capacidad del Buffer es 0." << endl;
    }

    // Inicialización de Semaforos
    sem_init(&fulls, 0, 0);
    sem_init(&emptys, 0, BC);
    // CReacion de arreglos de hebras que seran los productores y los consumidores
    thread consumidor[NC];
    thread productor[NP];
    string data;
    
    // Crea todos los hilos de los productores 
    for(int i = 0; i < NP; i++){
        productor[i] = thread(func_productor, i);
        data = "# Productor " + to_string(i+1) + " creado.";
        cout << data << endl;
    }

    // Crea todos los hilos de los consumidores 
    for (int i = 0; i < NC; i++){
        consumidor[i] = thread(func_consumidor, i);
        data = "# Consumidor " + to_string(i+1) + " creado.";
        cout << data << endl;
    }
    
    // Espera a que todos los hilos de los productores terminen su trabajo
    for(int i = 0; i < NP; i++){
        productor[i].join();
    }

    // Espera a que todos los hilos de los consumidores terminen su trabajo
    for (int i = 0; i < NC; i++){
        consumidor[i].join();
    }
    cout << "La cantidad de productos en el buffer es: " << buffer.size() << endl;
    // Destruir los semaforos
    sem_destroy(&fulls);
    sem_destroy(&emptys);
    cout << "* * * * Programa Terminado * * * *" << endl;
    return 0;
}
