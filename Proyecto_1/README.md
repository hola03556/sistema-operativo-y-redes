# Proyecto 1 - Sistemas Operativos y Redes
Integrantes: Renata Arcos - Bastián Morales

### Descripción del problema
El problema del productor-consumidor se refiere a una situación en la que un conjunto de threads productores producen elementos y los colocan en un buffer compartido, mientras que un conjunto de threads consumidores consumen estos elementos del buffer. El problema radica en cómo asegurar que los productores no sobrescriben elementos que aún no han sido consumidos por los consumidores, y viceversa, cómo asegurar que los consumidores no intenten consumir elementos que aún no han sido producidos. 

### Objetivo
Este proyecto implementa el problema del productor-consumidor utilizando threads, semáforos y mutex. El objetivo es simular la producción y consumo de productos en un sistema en el que múltiples productores generan productos y múltiples consumidores los consumen.

### Consideración de un buen funcionamiento del programa
- Se muestra en pantalla cada accion que se van ejecutando hasta que termine el programa.
- Los parámetros recibidos por la linea de comandos deben ser de esta forma sin excepciones:
    >Ejemplo: ./ejecutable NP NC BC NPP NCC
    > - NP : Número de productores.
    > - NC : Número de consumidores.
    > - BC : Tamaño del buffer.
    > - NPP : Número de productos a producir por cada productor.
    > - NCC : Número de productos a producir por cada consumidor.

- Los parárametros deben tener ciertos requisitos o el programa no se ejecutará:
    > - La capacidad de almacenamiento del buffer debe ser distinto de 0.
    > - Que se cumpla el ingreso de los 6 argumentos anteriormente mencionados.
    > - La cantidad de productos totales no exceda la suma de los productos a consumir más la capacidad limitada del buffer o contenedor. 
    > -  La cantidad de productos a elaborar debe ser al menos suficiente para satisfacer a los consumidores.

