/*Integrantes
    Renata Arcos 
    Bastián Morales
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/errno.h>

#define N 3

int main(){

    int h1 = fork(); // Padre - Crea Hijo N°1
    if (h1 == 0){ // Entra Hijo N°1
        printf("H1 Creado\n");
        int h2 = fork(); // Hijo N°1 asciende - Crea Hijo N°2 
        
        if (h2 == 0){ // Entra Hijo N°2
            printf("H2 Creado\n");
            int h3 = fork(); // Hijo N°2 asciende - Crea Hijo N° 3
            
            if (h3 == 0){ // Entra Hijo N°3
                printf("H3 Creado\n");
                printf("H3 Termina\n");
                return 0;
            }
            printf("H2 Termina\n");
            return 0;
        }
        printf("H1 Termina\n");
        return 0;
    }
    return 0;

}