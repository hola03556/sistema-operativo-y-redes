/*Integrantes
    Renata Arcos 
    Bastián Morales
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/errno.h>

#define N 3

int main(){
    pid_t hijos[N]; //aquí se guardarán los hijos creados
    int i;

    for (i = 0; i < N; i++){
        hijos[i] = fork(); //se crea el hijo

        if( hijos[i] == 0){ //el padre no entra porque su pid no es 0
            printf("H%d Creado!\n", i+1);
            break; //se rompe para que en el siguiente ciclo solo el padre cree otro hijo
        }
    }
    return 0;
}